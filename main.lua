require "vector"
require "boundaries.line"
require "boundaries.boundary"
require "user"
require "ray_tracing.ray"
require "ray_tracing.ray_tracing"
require "ray_tracing.intersection"

local boundaries = {}
local user
local rayTracing

local function drawVisibleArea(intersections)
    local points = { user.location.x, user.location.y }

    for _, intersection in pairs(intersections) do
        table.insert(points, intersection.location.x)
        table.insert(points, intersection.location.y)
    end

    table.insert(points, user.location.x)
    table.insert(points, user.location.y)

    love.graphics.line(unpack(points))

    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(0.2, 1, 0.2)
    for i = 1, #intersections do
        love.graphics.circle("fill", intersections[i].location.x, intersections[i].location.y, 4)
    end
    love.graphics.setColor(r, g, b, a)
end

function love.load()
    boundaries = {
        Boundary:new({
            Vector:new(0, 0),
            Vector:new(love.graphics.getWidth(), 0),
            Vector:new(love.graphics.getWidth(), love.graphics.getHeight()),
            Vector:new(0, love.graphics.getHeight()),
            Vector:new(0, 0)
        }),
        Boundary:new({
            Vector:new(200, 300),
            Vector:new(300, 200),
            Vector:new(150, 240)
        }),
        Boundary:new({
            Vector:new(400, 250),
            Vector:new(400, 350)
        }),
        Boundary:new({
            Vector:new(600, 440),
            Vector:new(700, 430),
            Vector:new(710, 540),
            Vector:new(600, 540),
            Vector:new(600, 440)
        }),
        Boundary:new({
            Vector:new(600, 40),
            Vector:new(700, 30),
            Vector:new(650, 150),
            Vector:new(710, 240),
            Vector:new(600, 240),
            Vector:new(600, 140)
        }),
    }

    user = User:new(Vector:new(300, 100))

    rayTracing = RayTracing:new(user.location, 30)
end

function love.update(dt)
    user:update(dt)
    rayTracing:update(dt, boundaries)
end

function love.draw()
    for _, boundary in ipairs(boundaries) do
        boundary:draw()
    end

    user:draw()

    local intersections = rayTracing.intersections
    drawVisibleArea(intersections)

    rayTracing:draw()
end
