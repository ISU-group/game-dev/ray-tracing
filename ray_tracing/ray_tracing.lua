RayTracing = {}
RayTracing.__index = RayTracing


function RayTracing:new(lightLocation, angleView)
    local rayTracing = {}
    setmetatable(rayTracing, RayTracing)

    rayTracing.location = lightLocation
    rayTracing.angleView = angleView -- in degrees
    rayTracing.directionAngle = 0 -- in degrees

    rayTracing.intersections = {}
    rayTracing.rays = {}

    return rayTracing
end

function RayTracing:update(dt, boundaries)
    self:followMouse()
    self:updateIntersections(boundaries)
end

function RayTracing:draw()
    local ray = Ray:new(self.location, self.directionAngle)
    ray:draw()
end

function RayTracing:followMouse()
    local x, y = love.mouse.getPosition()
    x = x - self.location.x
    y = y - self.location.y

    self.directionAngle = math.deg(math.atan2(y, x))
end

function RayTracing:updateIntersections(boundaries)
    self.intersections = {}
    local uniquePoints = self:getUniqueBoundariesPoints(boundaries)
    local uniqueAngles = self:getUniqueAngles(uniquePoints)
    local halfAngleView = self.angleView / 2

    table.insert(uniqueAngles, -halfAngleView + self.directionAngle)
    table.insert(uniqueAngles, halfAngleView + self.directionAngle)

    for _, angle in ipairs(uniqueAngles) do
        local ray = Ray:new(self.location, angle)
        local closestIntersect = nil
        local distance = 1 / 0

        for _, boundary in pairs(boundaries) do
            for _, line in pairs(boundary.lines) do
                local intersect = ray:getIntersectionPoint(line)

                if intersect then
                    local rayLength = (intersect.location - self.location):mag()
                    if rayLength < distance then
                        distance = rayLength
                        closestIntersect = intersect
                    end
                end
            end
        end

        if closestIntersect then
            table.insert(self.intersections, closestIntersect)
        end
    end

    table.sort(self.intersections, function(a, b)
        return a.angle < b.angle
    end)
end

function RayTracing:getUniqueAngles(points)
    local angles = {}
    local angleDeviation = 0.0001
    local halfAngleView = self.angleView / 2

    for _, point in pairs(points) do
        local angle = math.deg(math.atan2(point.y - self.location.y, point.x - self.location.x))

        if self.directionAngle - halfAngleView <= angle and angle <= self.directionAngle + halfAngleView then
            table.insert(angles, angle - angleDeviation)
            table.insert(angles, angle)
            table.insert(angles, angle + angleDeviation)
        end
    end

    return angles
end

function RayTracing:getUniqueBoundariesPoints(boundaries)
    local points = {}

    for _, boundary in pairs(boundaries) do
        for _, line in pairs(boundary.lines) do
            local key = tostring(line.a)

            if points[key] == nil then
                points[key] = line.a
            end

            key = tostring(line.b)

            if points[key] == nil then
                points[key] = line.b
            end
        end
    end
    return points
end
