Intersection = {}
Intersection.__index = Intersection

function Intersection:new(location, angle)
    local intersection = {}
    setmetatable(intersection, Intersection)

    intersection.location = location
    intersection.angle = angle

    return intersection
end
