Ray = {}
Ray.__index = Ray

function Ray:new(location, angle)
    local ray = {}
    setmetatable(ray, Ray)

    ray.location = location
    ray.angle = angle

    return ray
end

function Ray:draw()
    local point = self.location + Vector:from_angle(self.angle) * 40

    local r, g, b, a = love.graphics.getColor()
    local w = love.graphics.getLineWidth()
    love.graphics.setLineWidth(2 * w)
    love.graphics.setColor(1.0, 0.5, 0.6)

    love.graphics.line(self.location.x, self.location.y, point.x, point.y)

    love.graphics.setLineWidth(w)
    love.graphics.setColor(r, g, b, a)
end

function Ray:getIntersectionPoint(line)
    local x1 = line.a.x
    local y1 = line.a.y
    local x2 = line.b.x
    local y2 = line.b.y

    local direction = Vector:from_angle(self.angle)
    local x3 = self.location.x
    local y3 = self.location.y
    local x4 = self.location.x + direction.x
    local y4 = self.location.y + direction.y

    local denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    if denominator == 0 then
        return nil
    end

    local t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denominator
    local u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / denominator

    if t > 0 and t < 1 and u > 0 then
        return Intersection:new(
            Vector:new(
                x1 + t * (x2 - x1),
                y1 + t * (y2 - y1)
            ),
            self.angle
        )
    end

    return nil
end
