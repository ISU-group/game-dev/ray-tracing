Boundary = {}
Boundary.__index = Boundary

function Boundary:new(points)
    assert(#points > 1, "Boundary must have at least 2 points");

    local boundary = {}
    setmetatable(boundary, Boundary)

    boundary.lines = {}
    for i = 1, #points - 1 do
        table.insert(boundary.lines, Line:new(points[i], points[i + 1]))
    end

    return boundary
end

function Boundary:draw()
    for _, line in ipairs(self.lines) do
        line:draw()
    end
end
