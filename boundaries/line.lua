Line = {}
Line.__index = Line

function Line:new(a, b)
    local line = {}

    line.a = a
    line.b = b

    setmetatable(line, Line)
    return line
end

function Line:draw()
    local r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, 0.125)

    love.graphics.line(self.a.x, self.a.y, self.b.x, self.b.y)

    love.graphics.setColor(r, g, b, a)
end
