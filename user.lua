User = {}
User.__index = User

local userRadius = 6
local userSpeed = 5

function User:new(location)
    local user = {}
    setmetatable(user, User)

    user.location = location
    user.direction = Vector:new(1, 0)

    return user
end

function User:update(dt)
    self:move()
end

function User:draw()
    love.graphics.circle("fill", self.location.x, self.location.y, userRadius)
end

function User:move()
    if love.keyboard.isDown("w") then
        self.location.y = self.location.y - userSpeed
    end
    if love.keyboard.isDown("s") then
        self.location.y = self.location.y + userSpeed
    end
    if love.keyboard.isDown("d") then
        self.location.x = self.location.x + userSpeed
    end
    if love.keyboard.isDown("a") then
        self.location.x = self.location.x - userSpeed
    end
end
